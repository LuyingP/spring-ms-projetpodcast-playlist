package fr.isika.podcastService.playlist;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParseException;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.isika.podcastService.playlist.controller.MyPlaylistController;
import fr.isika.podcastService.playlist.entity.MyPlaylist;
import fr.isika.podcastService.playlist.repository.MyPlaylistRepository;
import org.mockito.junit.jupiter.MockitoExtension;
@ExtendWith(MockitoExtension.class)
// @RunWith(MockitoJUnitRunner.Silent.class)
@AutoConfigureMockMvc
//@SpringBootTest
@WebMvcTest(MyPlaylistController.class)

public class PlaylistApplicationTests {

	@MockBean
	private MyPlaylistRepository myPlaylistRespository;

	@Autowired
	private MockMvc mvc;

	

	@Test
	public void creatUserPlaylist() throws Exception {
		MyPlaylist savem1 = new MyPlaylist(1, 5, "ABC");
		String inputJson = mapToJson(savem1);
		when(myPlaylistRespository.save(any(MyPlaylist.class))).thenReturn(savem1);
		String uri = "/userlist";
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(201, status);

		String content = mvcResult.getResponse().getRedirectedUrl();

		assertEquals(content, "http://localhost/userlist/5");

	}

	private String mapToJson(Object obj) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(obj);
	}

	private <T> T mapFromJson(String json, Class<T> clazz)
			throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(json, clazz);
	}

	@Test
	public void getPlaylistByUserId() throws Exception {
		MyPlaylist savem1 = new MyPlaylist(1, 2, "ABC;OIE;");
		Integer userid = 2;
		String uri = "/myplaylist/" + userid;
		when(myPlaylistRespository.findByUserid(userid)).thenReturn(savem1);
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);

		String content = mvcResult.getResponse().getContentAsString();
		assertEquals(content, "ABC;OIE;");

	}

	@Test
	public void updateMyplaylist() throws Exception {
		MyPlaylist savem1 = new MyPlaylist(1, 3, "ZEF;KFK;");
		savem1.setPlaylist("IKL");
		Integer userid = 3;
		String uri = "/myplaylist/" + userid;

		when(myPlaylistRespository.findByUserid(userid)).thenReturn(savem1);
		String inputJson = mapToJson(savem1);

		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);

	}



	@Test
	public void deleteOneEpisodeFromMyPlaylist() throws Exception {
		MyPlaylist savem1 = new MyPlaylist(1, 4, "ZEF;KFK;");
		Integer userid = 4;
		String epi_id = "KFK";
		String uri = "/myplaylist/" + userid + "/"+epi_id;

		when(myPlaylistRespository.findByUserid(userid)).thenReturn(savem1);
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
		
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		
		String content = mvcResult.getResponse().getContentAsString();
		assertEquals(content,"[\"ZEF\"]");
	}
	
	
//	@Test
//	public void updateMyplaylistFailure() throws Exception {
//		MyPlaylist savem1 = new MyPlaylist(1, 3, "ZEF;KFK;");
//
//		Integer userid = 3;
//		String uri = "/myplaylist/" + userid;
//
//		when(myPlaylistRespository.findByUserid(userid)).thenReturn(savem1);
//		String inputJson = mapToJson(savem1);
//
//		String newlist = savem1.getPlaylist() + "KFK";
//		savem1.setPlaylist(newlist);
//
//		MvcResult mvcResult = mvc.perform(
//				MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
//				.andReturn();
//
//		int status = mvcResult.getResponse().getStatus();
//		System.out.println(status);
//
//	}

}
