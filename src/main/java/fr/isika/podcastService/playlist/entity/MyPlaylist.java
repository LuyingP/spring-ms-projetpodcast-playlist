package fr.isika.podcastService.playlist.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class MyPlaylist {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private Integer userid;
	@Lob
	private String playlist;

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public String getPlaylist() {
		return playlist;
	}

	public void setPlaylist(String playlist) {
		this.playlist = playlist;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public MyPlaylist() {
	}

	public MyPlaylist(Integer id, Integer userid, String playlist) {

		this.id = id;
		this.userid = userid;
		this.playlist = playlist;
	}
	
	

	public MyPlaylist(Integer userid, String playlist) {
		super();
		this.userid = userid;
		this.playlist = playlist;
	}

	@Override
	public String toString() {
		return "MyPlaylist [id=" + id + ", userid=" + userid + ", playlist=" + playlist + "]";
	}

	@Override
	public int hashCode() {
	
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + ((playlist != null) ? playlist.hashCode() : 0);
		result = 31 * result + ((userid != null) ? userid.hashCode() : 0 );
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
			
		if (obj == null) return false;
			
//		if (getClass() != obj.getClass())
//			return false;
		MyPlaylist other = (MyPlaylist) obj;
		
		if (id != null ? !id.equals(other.id) : other.id != null) return false;
        if (userid != null ? !userid.equals(other.userid) : other.userid != null) return false;
        return playlist != null ? playlist.equals(other.playlist) : other.playlist == null;
	}
	
	

}
