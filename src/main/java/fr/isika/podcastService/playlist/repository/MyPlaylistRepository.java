package fr.isika.podcastService.playlist.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import fr.isika.podcastService.playlist.entity.MyPlaylist;

public interface MyPlaylistRepository extends CrudRepository<MyPlaylist, Integer> {
	
	public Optional<MyPlaylist> findById(Integer userid);

	public MyPlaylist findByUserid(Integer userid);

	public boolean existsByUserid(Integer userid);

}