package fr.isika.podcastService.playlist.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import fr.isika.podcastService.playlist.entity.MyPlaylist;
import fr.isika.podcastService.playlist.repository.MyPlaylistRepository;

@RestController
@CrossOrigin("https://angular-podcast.herokuapp.com/")
public class MyPlaylistController {

	@Autowired
	private MyPlaylistRepository myplaylistRepository;
	
	@Value("${app.message}")
	private String welcomeMessage;
	
	@GetMapping("/welcome")
	public String getDataBaseConnectionDetails() {
		return welcomeMessage;
	}


	@PostMapping("/userlist")
	public ResponseEntity<?> ajouterDBafterRegistrer(@RequestBody MyPlaylist req) {
		if (myplaylistRepository.existsByUserid(req.getUserid())) {
			return ResponseEntity.badRequest().build();

		} else {
			myplaylistRepository.save(req);
			URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{userid}").buildAndExpand(req.getUserid())
					.toUri();

			return ResponseEntity.created(uri).build();
		}

	}

	@PutMapping("/myplaylist/{userid}")
	public MyPlaylist ajouterDansMyList(@PathVariable Integer userid, @RequestBody String epiId) throws Exception {
//		System.out.println("************* id user" + userid + " epi fa " + epiId);
		MyPlaylist list = myplaylistRepository.findByUserid(userid);
		String listEpis = list.getPlaylist();
//		System.out.println("@@@@@@@"+listEpis);
		if (listEpis!=null) {
			String[] arrayString = listEpis.split(";");
			for (String string : arrayString) {
				if (string.equalsIgnoreCase(epiId)) {
					throw new Exception("exited");
				}
			}
			
		} else {
			list.setPlaylist(list.getPlaylist() + ";" + epiId);
			myplaylistRepository.save(list);
			return list;
		}

		list.setPlaylist(list.getPlaylist() + ";" + epiId);
		myplaylistRepository.save(list);
		return list;
		
	}

	@GetMapping("/myplaylist/{userid}")
	public String getMyList(@PathVariable Integer userid) {
		System.out.println("get methode userid " + userid);
		MyPlaylist list = myplaylistRepository.findByUserid(userid);
		return list.getPlaylist();
	}

	@DeleteMapping("/myplaylist/{userid}/{epi_id}")
	public String[] deleteOneFavorie(@PathVariable Integer userid, @PathVariable String epi_id) {
		MyPlaylist list = myplaylistRepository.findByUserid(userid);
		String listEpiString = list.getPlaylist();
		String[] arrayString = listEpiString.split(";");
		List<String> myList = new ArrayList<String>(Arrays.asList(arrayString));
		myList.remove(epi_id);
		arrayString = myList.toArray(new String[0]);
		String newString = "";

		for (int i = 0; i < arrayString.length; i++) {
			if (i == arrayString.length - 1) {
				newString = newString + arrayString[i];
			} else {
				newString = newString + arrayString[i] + ";";
			}

		}
		list.setPlaylist(newString);
		myplaylistRepository.save(list);
		return arrayString;
	}

}
