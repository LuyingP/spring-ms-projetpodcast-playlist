package fr.isika.podcastService.playlist.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ConfigurationProperties("spring.datasource")
public class DBConfiguration {
	private String url;
	private String username;
	private String password;


	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Profile("docker")
	@Bean
	public String devDatabaseConnection() {
		System.out.println("DB connection for docker - CONTAINER");
		System.out.println(username);
		System.out.println(password);
		System.out.println(url);
		return "DB connection for docker - CONTAINER";
	}

	@Profile("local")
	@Bean
	public String prodDatabaseConnection() {
		System.out.println("DB Connection to mysql - local mysql");
		System.out.println(username);
		System.out.println(password);
		System.out.println(url);
		return "DB Connection to mysql - local mysql";
	}
	
	@Profile("deploy")
	@Bean
	public String deployDatabaseConnection() {
		System.out.println("DB Connection to herokou - herokou mysql");
		System.out.println(username);
		System.out.println(password);
		System.out.println(url);
		return "DB Connection to herokou - herokou mysql";
	}

}
